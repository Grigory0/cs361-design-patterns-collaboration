﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pattern333
{
    public class Vijiner
    {
        public string VijinerCoder(string text, string key)
        {
            if (key == string.Empty) { throw new ArgumentException(" Пустой ключ"); }

            string s = text;
            StringBuilder STR = new StringBuilder(s.Length);
            for (int i = 0; i < s.Length; i++)
            {
                if (Worker.alf.IndexOf(s[i]) < 0)
                    STR.Append(s[i]);
                else
                {
                    STR.Append(VijenerE(s[i], key, i));
                }
            }
            return STR.ToString();
        }

        public string VijinerDecoder(string text, string key)
        {
            if (key == string.Empty) { throw new ArgumentException(" Пустой ключ"); }
            string s = text;
            StringBuilder STR = new StringBuilder(s.Length);
            for (int i = 0; i < s.Length; i++)
            {
                if (Worker.alf.IndexOf(s[i]) < 0)
                    STR.Append(s[i]);
                else
                {
                    STR.Append(VijenerD(s[i], key, i));
                }
            }
            return STR.ToString();
        }

        public char VijenerE(char c, string key, int i)
        {
            int m = Worker.alf.Length;
            int j = Worker.alf.IndexOf(c);
            int _key = key[i % key.Length] - Worker.alf[0];
            return Caesar.CesarE(c, _key);
        }

        public char VijenerD(char c, string key, int i)
        {
            int m = Worker.alf.Length;
            int j = Worker.alf.IndexOf(c);
            int _key = key[i % key.Length] - Worker.alf[0];
            return Caesar.CesarD(c, _key);
        }
    }
}
