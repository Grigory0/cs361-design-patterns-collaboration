﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pattern333
{
    public class Worker
    {

        internal const string alf = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя";

        public AbstractTask Do(AbstractTask text)
        {
            Caesar cesar = new Caesar();
            Vijiner vijiner = new Vijiner();
            if (text.s() == 'e')
            {
                if (text.type == 1)
                    return new AbstractTask(cesar.Coder(text.text, int.Parse(text.key()), Caesar.CesarE), text.num(), "", 0, ' ');
                else
                    if (text.type == 2)
                        return new AbstractTask(vijiner.VijinerCoder(text.text, text.key()), text.num(), "", 0, ' ');
                //return new AbstractTask(Coder(text.text, text.key(), VijenerE), text.num(), "", 0, ' ');
            }
            else if (text.s() == 'd')
            {
                if (text.type == 1)
                    return new AbstractTask(cesar.Coder(text.text, int.Parse(text.key()), Caesar.CesarD), text.num(), "", 0, ' ');
                else
                    if (text.type == 2)
                        return new AbstractTask(vijiner.VijinerDecoder(text.text, text.key()), text.num(), "", 0, ' ');
                //return new AbstractTask(Coder(text.text, text.key(), VijenerD), text.num(), "", 0, ' ');
            }
            return text;
        }

        /*public string VijinerCoder(string text, string key)
        {
            if (key == string.Empty) { throw new ArgumentException(" Пустой ключ"); }

            string s = text;
            StringBuilder STR = new StringBuilder(s.Length);
            for (int i = 0; i < s.Length; i++)
            {
                if (alf.IndexOf(s[i]) < 0)
                    STR.Append(s[i]);
                else
                {
                    STR.Append(VijenerE(s[i], key, i));
                }
            }
            return STR.ToString();
        }

        public string VijinerDecoder(string text, string key)
        {
            if (key == string.Empty) { throw new ArgumentException(" Пустой ключ"); }
            string s = text;
            StringBuilder STR = new StringBuilder(s.Length);
            for (int i = 0; i < s.Length; i++)
            {
                if (alf.IndexOf(s[i]) < 0)
                    STR.Append(s[i]);
                else
                {
                    STR.Append(VijenerD(s[i], key, i));
                }
            }
            return STR.ToString();
        }

        public delegate char ConvertChar(char c, int i);
        public delegate char ConvertCharKey(char c, string key);

        public char CesarE(char c, int key)
        {
            int m = alf.Length;
            int j = alf.IndexOf(c);
            int temp = (j + key) % m;
            return alf[temp];
        }

        public char CesarD(char c, int key)
        {
            int m = alf.Length;
            int j = alf.IndexOf(c);
            int temp = j - key;
            while (temp < 0)
                temp += m;
            return alf[temp];
        }


        public char VijenerE(char c, string key, int i)
        {
            int m = alf.Length;
            int j = alf.IndexOf(c);
            int _key = key[i % key.Length] - alf[0];
            return CesarE(c, _key);
        }

        public char VijenerD(char c, string key, int i)
        {
            int m = alf.Length;
            int j = alf.IndexOf(c);
            int _key = key[i % key.Length] - alf[0];
            return CesarD(c, _key);
        }


        public string Coder(string text, int key, ConvertChar f1)
        {

            string s = text;
            StringBuilder STR = new StringBuilder(s.Length);
            for (int i = 0; i < s.Length; i++)
            {
                if (alf.IndexOf(s[i]) < 0)
                    STR.Append(s[i]);
                else
                {
                    STR.Append(f1(s[i], key));
                }
            }
            return STR.ToString();
        }
        */


    }

}
