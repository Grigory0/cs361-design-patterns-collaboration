﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pattern333
{
    public class AbstractTask
    {
        protected string _t;
        protected int _i;
        protected string _key;
        protected int _type;
        protected char _s;

        public AbstractTask(string t, int i, string key, int type, char s)
        {
            _t = t;
            _i = i;
            _key = key;
            _type = type;
            _s = s;
        }

        public string text { get { return _t; } }
        public int type { get { return _type; } }
        public int num() { return _i; }
        public string key() { return _key; }
        public char s() { return _s; }
    }
}
