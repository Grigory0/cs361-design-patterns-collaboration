﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Pattern333
{
    class Program
    {

        public string Concat(List<AbstractTask> t)
        {
            List<AbstractTask> tt = new List<AbstractTask>(t);
            tt.Sort(delegate(AbstractTask p1, AbstractTask p2)
            { return p1.num().CompareTo(p2.num()); });
            string text = null;
            foreach (AbstractTask elem in tt)
            {
                text += elem.text;
            }

            return text;
        }

        static string ReadFile(string file)
        {
            FileInfo f = new FileInfo(file);
            StreamReader r = f.OpenText();
            string text = r.ReadToEnd();
            r.Close();
            return text;
        }
        static void WriteFile(string file, string text)
        {
            FileInfo f = new FileInfo(file);
            StreamWriter w = f.CreateText();
            w.Write(text);
            w.Close();
        }

        public void TextCh(int N, string file, string file2, string key, int type, char c)
        {

            string text = ReadFile(file);

            List<AbstractTask> Tasks = TaskFactory.CreateTasks(text, 5, key, type, c);
            List<AbstractTask> TasksCode = new List<AbstractTask>();

            //int N = Tasks.Count;
            Worker[] ww = new Worker[N];
            for (int j = 0; j < N; ++j)
            {
                ww[j] = new Worker();
            }

            int i = 0;

            while (Tasks.Count > 0)
            {
                TasksCode.Add(ww[i].Do(Tasks[0]));
                Tasks.RemoveAt(0);
                i = i < N ? i + 1 : 0;
            }

            string nnn = Concat(TasksCode);

            WriteFile(file2, nnn);
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Количество рабочих = ");
            string N = Console.ReadLine();


            int Nc = -1;
            m1:
            if (!int.TryParse(N, out Nc) || Nc <= 0)
            {
                Console.WriteLine("Ошибка! Повторите ввод!");
                N = Console.ReadLine();
                goto m1;
            }

            m4:
            Console.WriteLine("Введите файл для чтения");
            string f = Console.ReadLine();
            if (!File.Exists(f))
            {
                Console.WriteLine("Ошибка: Файл не существует! Повторите ввод");
                goto m4;
            }
            else if (Path.GetExtension(f) == string.Empty)
            {
                Console.WriteLine("Ошибка: Недопустимое расширение файла! Повторите ввод");
                goto m4;
            }

            m5:
            Console.WriteLine("Введите файл для записи");
            string f2 = Console.ReadLine();
            if (Path.GetExtension(f2) == string.Empty)
            {
                Console.WriteLine("Ошибка: Недопустимое расширение файла! Повторите ввод");
                goto m5;
            }

            Console.WriteLine("Выбирите кодировку: Цезарь - 1, Виженер - 2");
            int type = Convert.ToInt32(Console.ReadLine());

        m2:
            if ((type!=1) && (type!=2))
            {
                Console.WriteLine("Ошибка! Повторите ввод!");
                type = Convert.ToInt32(Console.ReadLine());
                goto m2;
            }

            m6:
            Console.WriteLine("Введите ключ");
            string key = Console.ReadLine();

            int tmp;
            if (type==1 && !int.TryParse(key, out tmp))
            {
                Console.WriteLine("Ошибка: неверный ключ! Ключ должен быть целым числом. Повторите ввод!");
                goto m6;
            }
            else if (type == 2 && !key.All(xc => Worker.alf.Contains(xc)))
            {
                Console.WriteLine("Ошибка: неверный ключ! Ключ должен быть строкой символов из алфавита. Повторите ввод!");
                goto m6;
            }

            Console.WriteLine("Для шифрования введите e, для дешифрования d");
            char c = (char)Console.Read();

        m3:
            if ((c != 'e') && (c != 'd'))
            {
                Console.WriteLine("Ошибка! Повторите ввод!");
                c = (char)Console.Read();
                goto m3;
            }

            

            var x = new Program();
            x.TextCh(Nc, f, f2, key, type, c);
        }
    }
}
