﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pattern333
{
    public class Caesar
    {
        public string Coder(string text, int key, ConvertChar f1)
        {

            string s = text;
            StringBuilder STR = new StringBuilder(s.Length);
            for (int i = 0; i < s.Length; i++)
            {
                if (Worker.alf.IndexOf(s[i]) < 0)
                    STR.Append(s[i]);
                else
                {
                    STR.Append(f1(s[i], key));
                }
            }
            return STR.ToString();
        }

        public delegate char ConvertChar(char c, int i);
        public delegate char ConvertCharKey(char c, string key);

        public static char CesarE(char c, int key)
        {
            int m = Worker.alf.Length;
            int j = Worker.alf.IndexOf(c);
            int temp = (j + key) % m;
            return Worker.alf[temp];
        }

        public static char CesarD(char c, int key)
        {
            int m = Worker.alf.Length;
            int j = Worker.alf.IndexOf(c);
            int temp = j - key;
            while (temp < 0)
                temp += m;
            return Worker.alf[temp];
        }
    }
}
