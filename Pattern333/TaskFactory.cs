﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pattern333
{
    public class TaskFactory
    {
        public static List<AbstractTask> CreateTasks(string text, int m, string key, int type, char s) //factory
        {
            List<AbstractTask> result = new List<AbstractTask>();
            int tl = text.Length / m;
            for (int i = 0; i < m-1; i++)
            {
                result.Add(new AbstractTask(text.Substring(i * tl, tl), i, key, type, s));
            }
            result.Add(new AbstractTask(text.Substring((m-1) * tl, text.Length - tl * (m-1)), m-1, key, type, s));
            return result;
        }
    }
}
