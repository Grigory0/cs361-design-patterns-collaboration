﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pattern333;

namespace UnitTest_Pattern333
{
    [TestClass]
    
    public class UnitTest1
    {
        [TestMethod]
        public void CesarEncode()
        {
            var c = new Worker();

            var res = c.Coder("Тест работы", 6, c.CesarE);

            Assert.AreEqual("Шкчш цёжфшБ", res);
        }
        [TestMethod]
        public void CesarEncode2()
        {
            var c = new Worker();

            var res = c.Coder("Тест работы", 0, c.CesarE);

            Assert.AreEqual("Тест работы", res);
        }
        [TestMethod]
        public void CesarEncode3()
        {
            var c = new Worker();

            var res = c.Coder("ШИФР", 14, c.CesarE);

            Assert.AreEqual("ёЦвЮ", res);
        }

        [TestMethod]
        public void CesarDecode()
        {
            var c = new Worker();

            var res = c.Coder("Шкчш цёжфшБ", 6, c.CesarD);

            Assert.AreEqual("Тест работы", res);
        }

        [TestMethod]
        public void CesarDecode2()
        {
            var c = new Worker();

            var res = c.Coder("Шкчш цёжфшБ", 0, c.CesarD);

            Assert.AreEqual("Шкчш цёжфшБ", res);
        }
       
        [TestMethod]
        public void VijinerEncoder()
        {
            var c = new Worker();

            var res = c.VijinerCoder("Тест шифра", "КОД");

            Assert.AreEqual("Ьтхь ьтВфй", res);
        }

        [TestMethod]
        public void VijinerEncoder2()
        {
            var c = new Worker();

            var res = c.VijinerCoder("Тест шифра", "проверка");

            Assert.AreEqual("АУЮУ жСУЮО", res);
        }

        [TestMethod]
        public void VijinerEncoder3()
        {
            var c = new Worker();

            var res = c.VijinerCoder("Тест шифра", "а");

            Assert.AreEqual("сДРС ЧЗУПя", res);
        }


        [TestMethod]
        public void VijinerDecoder()
        {
            var c = new Worker();

            var res = c.VijinerDecoder("сДРС ЧЗУПя", "");

            Assert.AreEqual("Тест шифра", res);
        }

        [TestMethod]
        public void VijinerDecoder1()
        {
            var c = new Worker();

            var res = c.VijinerDecoder("сДРС ЧЗУПя", "а");

            Assert.AreEqual("Тест шифра", res);
        }



    }

    
}
